//fetch all todos then return titles
fetch('https://jsonplaceholder.typicode.com/todos')
.then(res => res.json())
.then(data => {
	const todosArr = [];
	data.map(filteredData => {
		// console.log(filteredData)
		todosArr.push(filteredData.title)
	})
	console.log(todosArr)
})

//fetch a single todo item
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(res => res.json())
.then(data => {
	let todo = {
		"title": data.title,
		"status": data.status
	}
	console.log(todo);
})

//create a todo item

fetch('https://jsonplaceholder.typicode.com/todos',{
	method: "POST",
	headers: {
		'Content-Type': "application/json"
	},
	body: JSON.stringify({
		"userId": 1,
	    "id": 201,
	    "title": "Moi",
	    "completed": false
	})
})
.then(res => res.json())
.then(data => {
	console.log(data);
})

//update a todo item

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: "PUT",
	headers: {
		'Content-Type': "application/json"
	},
	body: JSON.stringify({
		"userId": 6,
	    "title": "Moi",
	    "description": "CataAngelie",
	    "completed": false,
	    "dateCompleted": "tba"
	})
})
.then(res => res.json())
.then(data => {
	console.log(data);
})


//Update a todo item, change status to complete add date on status change

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: "PATCH",
	headers: {
		'Content-Type': "application/json"
	},
	body: JSON.stringify({
	    "completed": true,
	    "dateCompleted": new Date()
	})
})
.then(res => res.json())
.then(data => {
	console.log(data);
})


//Delete a todo item

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: "DELETE"
})
.then(res => res.json())
.then(data => {
	console.log(data);
})

